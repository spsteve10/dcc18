﻿using System;
using DCC18.Models;
using Newtonsoft.Json;
using System.Net.Http;

namespace DCC18.Services
{
    public class AccountService : WebApiService
    {
        public async void CreateAccount(string userId, string password)
        {
            string url = "http://webapi/route";

            var postData = new
            {
                id = userId,
                pwd = password
            };

            await Post<HttpResponseModel<bool>>(url, JsonConvert.SerializeObject(postData));
        }
    }
}
