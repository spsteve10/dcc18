﻿using System;
namespace DCC18.Services
{
    public interface IAppPropertiesService
    {
        string GetVersionNumber();
        string GetBuildNumber();
        string GetAppName();
    }
}
