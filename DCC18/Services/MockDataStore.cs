﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DCC18.Models;

namespace DCC18.Services
{
    public class MockDataStore : IDataStore<Item>
    {
        List<Item> items;

        public MockDataStore()
        {
            items = new List<Item>();
            var mockItems = new List<Item>
            {
                new Item { Id = "video", Text = "Launch Video Player", Description="A Custom Renderer and Type Converter example." },
                new Item { Id = "email", Text = "Email", Description="Send an email." },
                new Item { Id = "flashlight", Text = "Flashlight", Description="Toggle flashlight." },
                new Item { Id = "geolocation", Text = "Find my location", Description="Find my geo coordinates." },
                new Item { Id = "cachedgeolocation", Text = "Find my cached location", Description="Find my cached coordinates." }
            };

            foreach (var item in mockItems)
            {
                items.Add(item);
            }
        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            return await Task.FromResult(items);
        }
    }
}
