﻿using System;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace DCC18.Services
{
    public class FlashLightService
    {
        public async Task ToggleFlashLight()
        {
            try
            {
                // Turn On
                await Flashlight.TurnOnAsync();

                // Turn Off
                await Flashlight.TurnOffAsync();
            }
            catch (FeatureNotSupportedException fnsEx)
            {
                // Handle not supported on device exception
            }
            catch (PermissionException pEx)
            {
                // Handle permission exception
            }
            catch (Exception ex)
            {
                // Unable to turn on/off flashlight
            }
        }
    }
}
