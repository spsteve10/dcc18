﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using DCC18.Models;
using Newtonsoft.Json;

namespace DCC18.Services
{
    public abstract class WebApiService
    {
        private const string MediaTypeJSON = "application/json";
        private bool token = true;

        public async Task<HttpResponseModel<T>> Get<T>(string URL)
        {
            return await WebAPIExecute<T>(Method.GET, URL);
        }

        public async Task<HttpResponseModel<T>> Post<T>(string URL, string postString)
        {
            return await WebAPIExecute<T>(Method.POST, URL, postString);
        }

        private async Task<HttpResponseModel<T>> WebAPIExecute<T>(Method method, string URL, string postString = "")
        {

            var returnModel = new HttpResponseModel<T>();

            var responseModel = await Execute(method, URL, postString);

            if (responseModel.Error == null)
            {
                var content = await responseModel.Response.Content.ReadAsStringAsync();
                var returnObject = JsonConvert.DeserializeObject<T>(content);
                returnModel.Response = returnObject;
            }
            else
            {
                returnModel.Error = $"Http error: {responseModel.Error}";
            }
            return returnModel;
        }


        private async Task<HttpResponseModel<HttpResponseMessage>> Execute(Method method, string url, string postString)
        {
            var returnModel = new HttpResponseModel<HttpResponseMessage>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.Timeout = new TimeSpan(0, 0, 10);

                    if (token)
                    {
                        //DO SOMETHING WITH TOKEN
                    }
                    else
                    {
                        //FORCE TO LOGIN SCREEN
                        return new HttpResponseModel<HttpResponseMessage>();
                    }

                    switch (method)
                    {
                        case Method.GET:
                            client.DefaultRequestHeaders.Accept.Add(
                                new MediaTypeWithQualityHeaderValue(MediaTypeJSON));
                            returnModel.Response = await client.GetAsync(url);
                            break;
                        case Method.POST:
                            client.DefaultRequestHeaders.Accept.Add(
                                new MediaTypeWithQualityHeaderValue(MediaTypeJSON));
                            returnModel.Response =
                                await client.PostAsync(url,
                                                       new StringContent(postString, Encoding.UTF8, MediaTypeJSON));
                            break;
                        default:
                            throw new WebException();
                    }

                    return returnModel;
                }
            }
            catch (Exception ex)
            {
                returnModel.Error = ex.Message;
                return returnModel;
            }
        }

        private enum Method
        {
            GET,
            POST,
        }
    }
}
