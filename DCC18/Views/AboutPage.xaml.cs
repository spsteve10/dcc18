﻿using System;
using System.Collections.Generic;
using DCC18.Services;
using Xamarin.Forms;

namespace DCC18.Views
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();

            this.AppNameSpan.Text = DependencyService.Get<IAppPropertiesService>().GetAppName();
            this.VersionNumberSpan.Text = DependencyService.Get<IAppPropertiesService>().GetVersionNumber();
            this.BuildNumberSpan.Text = DependencyService.Get<IAppPropertiesService>().GetBuildNumber();
        }

    
    }
}
