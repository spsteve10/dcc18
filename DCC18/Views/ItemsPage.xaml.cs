﻿using System;
using System.Collections.Generic;
using DCC18.Models;
using DCC18.Services;
using DCC18.ViewModels;
using DCC18.Views;

using Xamarin.Forms;

namespace DCC18.Views
{
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = viewModel = new ItemsViewModel();
        }

        async void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as Item;
            if (item == null)
                return;

            switch(item.Id)
            {
                case "video":
                    await Navigation.PushAsync(new PlayWebVideoPage());
                    break;
                case "email":
                    await (new EmailService()).SendEmail("DCC18 APP", "In App email.", new List<string>() { "sonnymotest@gmail.com" });
                    break;
                case "flashlight":
                    await (new FlashLightService()).ToggleFlashLight();
                    break;
                case "geolocation":
                    await (new LocationService()).GetLocation(this);
                    break;
                case "cachedgeolocation":
                    await (new LocationService()).GetCachedLocation(this);
                    break;
                default:
                    break;

            }
           

            // Manually deselect item.
            ItemsListView.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (viewModel.Items.Count == 0)
                viewModel.LoadItemsCommand.Execute(null);
        }
    }
}
