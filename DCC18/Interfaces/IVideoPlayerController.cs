﻿using System;
namespace DCC18.Interfaces
{
    public enum VideoStatus
    {
        NotReady,
        Playing,
        Paused
    }

    public interface IVideoPlayerController
    {
        VideoStatus Status { set; get; }

        TimeSpan Duration { set; get; }
    }
}
