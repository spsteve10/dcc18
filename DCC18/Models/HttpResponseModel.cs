﻿using System;
namespace DCC18.Models
{
    public class HttpResponseModel<T>
    {
        public T Response
        {
            get; set;
        }

        public string Error
        {
            get; set;
        }
    }
}
