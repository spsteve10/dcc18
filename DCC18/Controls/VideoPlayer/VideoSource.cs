﻿using System;
using Xamarin.Forms;

namespace DCC18.Controls.VideoPlayer
{
    [TypeConverter(typeof(VideoSourceConverter))]
    public abstract class VideoSource : Element
    {
        public static VideoSource FromUri(string uri)
        {
            return new UriVideoSource() { Uri = uri };
        }
    }
}
