﻿using System;
using Xamarin.Forms;

namespace DCC18.Controls.VideoPlayer
{
    public class VideoSourceConverter : TypeConverter
    {
        public override object ConvertFromInvariantString(string value)
        {
            if (!String.IsNullOrWhiteSpace(value))
            {
                Uri uri;

                Uri.TryCreate(value, UriKind.Absolute, out uri);

                return VideoSource.FromUri(value);
            }

            throw new InvalidOperationException("Cannot convert null or whitespace to ImageSource");
        }
    }
}
