﻿using System;
namespace DCC18.Constants
{
    public static class AppConstants
    {
        public const string source = "https://bitbucket.org/spsteve10/dcc18/src";
        public const string xEssentials = "https://docs.google.com/document/d/1e8CQ-DUidKK0HyWsir8hu0awDOsm76C_NCeS2OX9HqQ/edit?usp=sharing";
        public const string xVS = "https://docs.google.com/document/d/1akopCLVgm3YCEcDyv_5l1IG9nD6HbuaknPfJZI-UZvc/edit?usp=sharing";
        public const string xForms = "https://docs.google.com/document/d/1F18b0FhLUGh-H9u5eGix9QreXM3AMTr4Mu2aLdm2kFc/edit?usp=sharing";
        public const string xFlying = "https://docs.google.com/document/d/11X1xEJCar96OvNLzek9uz4K-4VNLXFYUaRa55ZggjDw/edit?usp=sharing";
    }
}
