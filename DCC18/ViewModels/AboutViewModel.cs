﻿using System;
using System.Windows.Input;
using DCC18.Services;
using Xamarin.Forms;

namespace DCC18.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";

            OpenWebCommand = new Command<string>(OpenWeb);
        }

        public ICommand OpenWebCommand { get; private set; }

        private void OpenWeb(string link)
        {
            Device.OpenUri(new Uri(link));
        }
    }
}
