﻿using DCC18.iOS.Services;
using DCC18.Services;
using Foundation;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppPropertiesService))]

namespace DCC18.iOS.Services
{
    public class AppPropertiesService : IAppPropertiesService
    {
        public AppPropertiesService()
        {
        }

        public string GetAppName()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleDisplayName").ToString(); 
        }

        public string GetBuildNumber()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion").ToString();
        }

        public string GetVersionNumber()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString").ToString();
        }
    }
}
