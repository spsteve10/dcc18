﻿using System;
using DCC18.Droid.Service;
using DCC18.Services;
using Xamarin.Forms;

[assembly: Dependency(typeof(AppPropertiesService))]

namespace DCC18.Droid.Service
{
    public class AppPropertiesService : IAppPropertiesService
    {
        public AppPropertiesService()
        {
        }

        public string GetAppName()
        {
            return Android.App.Application.Context.PackageManager.GetApplicationLabel(Android.App.Application.Context.ApplicationInfo);
        }

        public string GetBuildNumber()
        {
            return Android.App.Application.Context.PackageManager.GetPackageInfo(Android.App.Application.Context.PackageName, 0).VersionCode.ToString();
        }

        public string GetVersionNumber()
        {
            return Android.App.Application.Context.PackageManager.GetPackageInfo(Android.App.Application.Context.PackageName, 0).VersionName;
        }
    }
}
