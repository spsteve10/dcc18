﻿using System;
using Xamarin.UITest;
using Xamarin.UITest.Queries;

namespace DCC18.UITests
{
    public class AppInitializer
    {
        public static IApp StartApp(Platform platform)
        {
            if (platform == Platform.Android)
            {
                IApp app = ConfigureApp
    .Android
    .ApkFile("/Users/sonnystevenson/Documents/DCC2018/DCC18/DCC18.Android/bin/Debug/com.DCodeCamp18.DCC18.apk")
    .StartApp();
                return app;
            }

            return ConfigureApp.iOS.StartApp();
        }
    }
}