# DCC18
Sample project for Desert Code Camp 2018.

### Prerequisites

Visual Studio for Mac 2017
Xamarin.Forms 3.0

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Several examples off microsoft's xamarin site were used in this sample.
